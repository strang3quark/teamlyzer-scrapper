package scrapper

import (
	"regexp"
	"strings"
	"time"

	"github.com/gocolly/colly"
)

func GetCompanyWorkReviewsMeta(company Company, callback OnReviewMetaFetched) {
	c := GetCollyCollector()

	c.OnXML(SCRAPPER_CONFIGURATION.XPathWorkReviewListLink, func(e *colly.XMLElement) {
		link := e.Attr("href")

		re := regexp.MustCompile("(?s)work-reviews/(.*?)/")

		review := Review{
			ID:      re.FindStringSubmatch(link)[1],
			Company: company,
			Title:   strings.Trim(e.Text, " "),
			Link:    link,
		}

		enrichWorkReview(&review)

		if callback != nil && !callback(review) {
			callback = nil
			// Detach XML
			c.OnXMLDetach(SCRAPPER_CONFIGURATION.XPathWorkReviewListLink)
			c.OnXMLDetach(SCRAPPER_CONFIGURATION.XPathPaginationLastLink)
		}
	})

	// Go to next page
	c.OnXML(SCRAPPER_CONFIGURATION.XPathPaginationLastLink, func(e *colly.XMLElement) {
		if callback != nil {
			time.Sleep(SCRAPPER_CONFIGURATION.SleepMillis * time.Millisecond)

			slug := e.Attr("href")
			c.Visit("https://pt.teamlyzer.com" + slug)
		}
	})

	c.Visit("https://pt.teamlyzer.com" + company.Link + "/work-reviews")
}

func enrichWorkReview(reviewMeta *Review) {
	c := GetCollyCollector()

	c.OnHTML(".body_review", func(h *colly.HTMLElement) {
		pros := h.DOM.ContentsFiltered(".voffset2").Text()
		cons := h.DOM.ContentsFiltered(".voffset5").Text()

		reviewMeta.Content = pros + cons
	})

	c.Visit("https://pt.teamlyzer.com" + reviewMeta.Link)
}
