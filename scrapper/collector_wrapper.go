package scrapper

import (
	"log"
	"os"

	"github.com/gocolly/colly"
)

const LOGIN_ADDRESS = "https://pt.teamlyzer.com/auth/login"
const EMAIL_ENV = "TEAMLYZER_EMAIL"
const PASSWORD_ENV = "TEAMLYZER_PASSWORD"

var gCollyCollector *colly.Collector = nil

func GetCollyCollector() *colly.Collector {

	if gCollyCollector == nil {
		collyCollector := colly.NewCollector()
		collyCollector.AllowURLRevisit = false

		login(collyCollector)

		gCollyCollector = collyCollector
		return gCollyCollector.Clone()
	}

	return gCollyCollector.Clone()
}

func login(collector *colly.Collector) {
	const CSRF = "//input[@name='csrf_token']"

	collector.OnXML(CSRF, func(x *colly.XMLElement) {
		csrf := x.Attr("value")

		collector.OnXMLDetach(CSRF)

		log.Println(x.Request.URL, "CSRF Token:", csrf)

		err := collector.Post(LOGIN_ADDRESS,
			map[string]string{
				"email":       os.Getenv(EMAIL_ENV),
				"password":    os.Getenv(PASSWORD_ENV),
				"remember_me": "y",
				"csrf_token":  csrf,
			})

		if err != nil {
			log.Fatal(err)
		}

		log.Println(collector.Cookies("https://pt.teamlyzer.com"))
	})

	collector.Visit(LOGIN_ADDRESS)
}
