package scrapper

type Review struct {
	Company Company
	ID      string
	Title   string
	Link    string
	Content string
}

type OnReviewMetaFetched func(Review) bool
