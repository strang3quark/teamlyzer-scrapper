package scrapper

import (
	"time"
)

type ScrapperConfigurationType struct {
	ClientEmail             string
	ClientPassword          string
	SleepMillis             time.Duration
	XPathCompanyListLink    string
	XPathPaginationLastLink string
	XPathWorkReviewListLink string
}

var SCRAPPER_CONFIGURATION = ScrapperConfigurationType{
	SleepMillis:             1,
	XPathCompanyListLink:    "//h3/a[starts-with(@href, '/companies/')]",
	XPathPaginationLastLink: "//ul[@class='pagination']/li[last()]/a",

	XPathWorkReviewListLink: "//h3/a[starts-with(@href, '/companies/') and contains(@href, 'work-reviews')]",
}
