package scrapper

import (
	"strings"
	"time"

	"github.com/gocolly/colly"
)

type Company struct {
	Name string
	Link string
}

type OnCompanyFetched func(Company) bool

func GetCompanies(callback OnCompanyFetched) {
	c := GetCollyCollector()

	c.OnXML(SCRAPPER_CONFIGURATION.XPathCompanyListLink, func(e *colly.XMLElement) {
		company := Company{
			Name: strings.Trim(e.Text, " "),
			Link: e.Attr("href"),
		}

		if callback != nil && !callback(company) {
			callback = nil
			// Detach XML
			c.OnXMLDetach(SCRAPPER_CONFIGURATION.XPathCompanyListLink)
			c.OnXMLDetach(SCRAPPER_CONFIGURATION.XPathPaginationLastLink)
		}
	})

	// Go to next page
	c.OnXML(SCRAPPER_CONFIGURATION.XPathPaginationLastLink, func(e *colly.XMLElement) {
		if callback != nil {
			time.Sleep(SCRAPPER_CONFIGURATION.SleepMillis * time.Millisecond)

			slug := e.Attr("href")
			c.Visit("https://pt.teamlyzer.com" + slug)
		}
	})

	c.Visit("https://pt.teamlyzer.com/companies/")
}
