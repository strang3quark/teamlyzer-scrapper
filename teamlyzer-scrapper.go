package main

import (
	"log"

	"brunojesus.pt/teamlyzer-scrapper/scrapper"
)

func main() {
	scrapper.GetCompanies(func(c scrapper.Company) bool {
		log.Println("Company:", c)

		scrapper.GetCompanyWorkReviewsMeta(c, func(w scrapper.Review) bool {
			log.Println("Review", w)
			return false
		})

		return false //do not continue
	})
}
